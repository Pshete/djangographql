from django.db import models
#from django.contrib.postgres.fields import ArrayField
from django.core.validators import int_list_validator
# Create your models here.

class Movie(models.Model):

    adult = models.BooleanField()
    poster_path = models.CharField(max_length=35,null=True)
    overview = models.TextField()
    release_date = models.DateField()
    genre_ids = models.TextField(validators=[int_list_validator])
    tmdb_id = models.IntegerField()
    original_title =  models.TextField()
    original_language = models.CharField(max_length=10)
    title = models.TextField()
    vote_count = models.IntegerField()
    popularlity = models.FloatField()
    video = models.BooleanField()
    keywords = models.TextField(null=True)

    def __str__(self):
        return self.title


class MoviesList(models.Model):
    codename = models.CharField(max_length=15,unique=True)
    movies = models.ManyToManyField('Movie',related_name='movies')
    listkeys = models.TextField(null=True)

    def __str__(self):
        return self.codename

        




