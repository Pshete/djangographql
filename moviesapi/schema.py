from django.db.models.query_utils import subclasses
import graphene
from graphene_django import DjangoObjectType
from moviesapi.models import Movie,MoviesList

class MovieType(DjangoObjectType):
    class Meta:
        model = Movie

class MoviesListType(DjangoObjectType):
    class Meta:
        model = MoviesList

#defines the mutation class
class CreateMoviesList(graphene.Mutation):
     
    # Defines the data you can send to the server
    class Arguments:
        codename = graphene.String()    #input to the query needed for resolving the mutation

    #output fields after the query runn
    codename = graphene.String()  #
    status  = graphene.String() 

    #mutation method
    def mutate(self,info,codename):
        try:
            ml =MoviesList(codename=codename)
            ml.save()
            status = "created"
        except:
            status = "List not reated"
        return CreateMoviesList(
            codename = ml.codename,
            status = status,
        )

class AddToList(graphene.Mutation):

    class Arguments:
        codename = graphene.String()
        movieid = graphene.Int()

    
    codename = graphene.String()
    movieid = graphene.Int()
    status = graphene.Boolean()
       


    def mutate(self,info,codename,movieid):
        try:
            li = MoviesList.objects.get(codename=codename)
            add_movie = Movie.objects.get(id=movieid)
            li.movies.add(add_movie)
            status = True
        except:
            status = False

        if li.listkeys!=None:
            unique_keys = set(li.listkeys.split(','))
        else:
            unique_keys = set()
        movie_keys = set(add_movie.keywords.split(','))

        similar_keys = list(unique_keys.union(movie_keys))
        li.listkeys = ','.join(similar_keys)
        li.save()

        return AddToList(
            codename = li.codename,
            movieid = add_movie.id,
            status = status,
        )
        



class Query(graphene.ObjectType):
    movies = graphene.List(MovieType)  #corresponds to a GraphQL query
    movie = graphene.Field(MovieType,m_id=graphene.Int())   #corresponds to a GraphQL query
    movielist = graphene.List(MovieType,codename=graphene.String()) #corresponds to a GraphQL querymovi
    movielists = graphene.List(MoviesListType)
    recommendations = graphene.List(MovieType,codename=graphene.String())


    #resolvers for all these fields is just fetching the data from the Models and returning it
    def resolve_movies(self,info,**kwargs):
        return Movie.objects.all()

    def resolve_movie(self,info,m_id=None):
        try:
            return Movie.objects.get(pk=m_id)
        except:
            return None

    def resolve_movielist(self,info,codename):
        try:
            return MoviesList.objects.get(codename=codename).movies.all()
        except:
            return None

    def resolve_movielists(self,info):
        return MoviesList.objects.all()

    def resolve_recommendations(self,info,codename=None):
        li = MoviesList.objects.get(codename=codename)
        listkeys = set(li.listkeys.split(','))

        def similar(movie):
            try:
                repeated = len(listkeys.intersection(set(movie.keywords.split(","))))
                common = len(listkeys.union(set(movie.keywords.split(","))))
                return float(repeated/common)
            except:
                return 0
        
        recommendations = sorted(Movie.objects.all(),key=similar)
        total_recommendations = len(recommendations)

        return recommendations[total_recommendations-1:total_recommendations-10:-1]


#It is also one of the root nodes also known as Root Mutation
class Mutation(graphene.ObjectType):
    movielist = CreateMoviesList.Field()    # define mutation as Fields in Root Mutation.
    add = AddToList.Field()
