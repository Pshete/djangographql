from django.core.management.base import BaseCommand
from django.utils import timezone
import tmdbsimple as tmdb
from moviesapi.models import Movie


class Command(BaseCommand):

    def handle(self,*args,**kwargs):
        tmdb.API_KEY = '984a96a43d63bc356eeb62b702d958cf'
        PAGES = 10

        for page in range(1,PAGES+1):
            popular = tmdb.Movies().popular(page=page,region='IN')
            for i in popular['results']:
                keywords = [j['name'] for j in tmdb.Movies(i['id']).keywords()['keywords']]
                m = Movie(
                    adult = i['adult'],
                    poster_path = i['poster_path'],
                    overview = i['overview'],
                    release_date = i['release_date'],
                    genre_ids = i['genre_ids'],
                    tmdb_id = i['id'],
                    original_title = i['original_title'],
                    original_language = i['original_language'],
                    title = i['title'],
                    vote_count = i['vote_count'],  
                    popularlity = i['popularity'],
                    video = i['video'],
                    keywords = keywords
                )
                m.save()
