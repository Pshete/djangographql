import graphene

import moviesapi.schema
from graphene_django import DjangoObjectType

class Query(moviesapi.schema.Query,graphene.ObjectType):
    pass

schema = graphene.Schema(query=Query,mutation=moviesapi.schema.Mutation)